﻿using System.Web;
using System.Web.Optimization;

namespace Sports_News
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/vendors.min.js",
                      "~/Scripts/ui/jquery.sticky.js",
                      "~/Scripts/ui/prism.min.js",
                      "~/Scripts/extensions/wNumb.js",
                      "~/Scripts/extensions/nouislider.min.js",
                      "~/Scripts/forms/select/select2.full.min.js",
                      "~/Scripts/app-menu.js",
                      "~/Scripts/app.js",
                      "~/Scripts/components.js",
                       "~/Scripts/app-ecommerce-shop.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                      "~/Scripts/data-list.js",
                      "~/Scripts/extensions/dropzone.min.js",
                      "~/Scripts/datatable/datatables.min.js",
                      "~/Scripts/datatable/datatables.buttons.min.js",
                      "~/Scripts/datatable/datatables.bootstrap4.min.js",
                      "~/Scripts/datatable/buttons.bootstrap.min.js",
                      "~/Scripts/datatable/dataTables.select.min.js",
                      "~/Scripts/datatable/datatables.checkboxes.min.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/editors").Include(
                      "~/Scripts/editors/quill/katex.min.js",
                      "~/Scripts/editors/quill/highlight.min.js",
                      "~/Scripts/editors/quill/quill.min.js",
                      "~/Scripts/extensions/jquery.steps.min.js",
                      "~/Scripts/forms/validation/jquery.validate.min.js",
                      "~/Scripts/editor-quill.js"
                      
                      ));

            //--------------------------------------------------------------
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/vendors.min.css",
                      "~/Content/nouislider.min.css",
                      "~/Content/prism.min.css",
                      "~/Content/select2.min.css",
                      "~/Content/bootstrap-extended.css",
                      "~/Content/colors.css",
                      "~/Content/components.css",
                      "~/Content/dark-layout.css",
                      "~/Content/semi-dark-layout.css",
                      "~/Content/horizontal-menu.css",
                      "~/Content/palette-gradient.css",
                      "~/Content/noui-slider.min.css",
                      "~/Content/app-ecommerce-shop.css",
                      "~/Content/style.css",
                      "~/Content/dropzone.css",
                      "~/Content/data-list-view.css",
                      "~/Content/vendors/css/file-uploaders/dropzone.min.css",
                      "~/Content/vendors/css/tables/datatable/datatables.min.css",
                      "~/Contentvendors/css/tables/datatable/extensions/dataTables.checkboxes.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/editors").Include(
                      "~/Content/vendors/css/editors/quill/katex.min.css",
                      "~/Content/vendors/css/editors/quill/monokai-sublime.min.css",
                      "~/Content/vendors/css/editors/quill/quill.snow.css",
                      "~/Content/vendors/css/editors/quill/quill.bubble.css"
                      ));
        }
    }
}
