/*=========================================================================================
	File Name: editor-quill.js
	Description: Quill is a modern rich text editor built for compatibility and extensibility.
	----------------------------------------------------------------------------------------
	Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
	Author: PIXINVENT
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
(function (window, document, $) {
  'use strict';

    var editor = CKEDITOR.editor.replace('my-editor');

    $("#identifier").on("submit", function () {
        var value1 = editor.getData();
        document.getElementById("hiddenArea").value = value1;
    })
   
})(window, document, jQuery);
