﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sports_News.Models
{
    public class MultipleModels
    {
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<Category> Categorys { get; set; }
    }
}