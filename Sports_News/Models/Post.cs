﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Sports_News.Models
{
    [Table("posts")]
    public class Post
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string title { get; set; }
        public int value_category { get; set; }
        public string describes { get; set; }
        public int views { get; set; }
        public string image { get; set; }
        public string body { get; set; }
        public byte published { get; set; }
        public DateTime created_at { get; set; }
    }
}