﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Sports_News.Models
{
    [Table("users")]
    public class User
    {
        public int id { get; set; }
        public string username { get; set; }
        [Required(ErrorMessage = "Email không được để trống!")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng!")]
        public string email { get; set; }
        public string full_name { get; set; }
        public int role { get; set; }
        [Required(ErrorMessage = "Password không được để trống!")]
        public string password { get; set; }
        public DateTime created_at { get; set; }
        public string img_url { get; set; }

    }
}