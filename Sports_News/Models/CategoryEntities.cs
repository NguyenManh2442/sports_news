﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Sports_News.Models
{
    public class CategoryEntities : DbContext
    {
        public CategoryEntities() : base("Sports_news")
        {
        }
        public DbSet<Category> Categories { get; set; }

        public int countCategory()
        {
            int count = 0;
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT COUNT(id) as countId FROM categories";

            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            while (rd.Read())
            {
                count = (int)rd["countId"];
            }
            con.Close();
            return count;
        }
        public List<Category> listCategory()
        {
            List<Category> listCate = new List<Category>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "SELECT name, id FROM categories";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            while (rd.Read())
            {
                Category c = new Category();
                c.name = (string)rd["name"];
                c.id = (int)rd["id"];
                listCate.Add(c);
            }
            con.Close();
            return listCate;
        }
        public bool addCategory(Category category)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Insert into categories(name) values(@name) ";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@name", category.name);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;
        }

        public bool editCategory(Category category)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = null;

            sql = "Update categories Set name = @name Where id = @id ";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", category.id);
            com.Parameters.AddWithValue("@name", category.name);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
        public bool deleteCategory(int id)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Delete from categories where id = @id";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", id);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
    }
}