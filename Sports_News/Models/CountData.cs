﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sports_News.Models
{
    public class CountData
    {
        public int CountUsers { get; set; }
        public int CountUserAdmin { get; set; }
        public int CountUserEmp { get; set; }
        public int CountPosts { get; set; }
        public int CountPostPublic { get; set; }
        public int CountPostPrivate { get; set; }
        public int CountCategorys { get; set; }
    }
}