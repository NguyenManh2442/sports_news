﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Sports_News.Models
{
    public class UserEntities : DbContext
    {
        public UserEntities() : base("Sports_news")
        {
        }
        public DbSet<User> Users { get; set; }

        public int countUser()
        {
            int count = 0;
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT COUNT(id) as countId FROM users";

            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            while (rd.Read())
            {
                count = (int)rd["countId"];
            }
            con.Close();
            return count;
        }
        public List<User> loginUser(User user)
        {
            List<User> inforUser = new List<User>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT * FROM users WHERE email = '"+ user.email + "'";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                User u = new User();
                u.id = (int)rd["id"];
                u.email = (string)rd["email"];
                u.username = (string)rd["username"];
                u.full_name = (string)rd["full_name"];
                u.password = (string)rd["password"];
                u.role = (int)rd["role"];
                u.img_url = (string)rd["img_url"];
                inforUser.Add(u);
                i++;
            }

            con.Close();
            if(i == 0)
            {
                return null;
            }

            return inforUser;
        }
        public bool addUserAdmin(User user)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Insert into users(email, username, full_name, role, password, created_at, img_url) values(@email, @username, @full_name, @role, @password, @created_at, @img_url) ";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@email", user.email);
            com.Parameters.AddWithValue("@username", user.username);
            com.Parameters.AddWithValue("@full_name", user.full_name);
            com.Parameters.AddWithValue("@role", user.role);
            com.Parameters.AddWithValue("@password", user.password);
            com.Parameters.AddWithValue("@created_at", user.created_at);
            com.Parameters.AddWithValue("@img_url", user.img_url);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;
        }

        public bool editUserAdmin(User user)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = null;
            if (user.password == null)
            {
                sql = "Update users Set email = @email, username = @username, full_name = @full_name, role = @role, created_at = @created_at, img_url = @img_url Where id = @id ";
            }
            else
            {
                sql = "Update users Set email = @email, username = @username, full_name = @full_name, role = @role, password = @password, created_at = @created_at, img_url = @img_url Where id = @id ";
            }
            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", user.id);
            com.Parameters.AddWithValue("@email", user.email);
            com.Parameters.AddWithValue("@username", user.username);
            com.Parameters.AddWithValue("@full_name", user.full_name);
            com.Parameters.AddWithValue("@role", user.role);
            if (user.password != null)
            {
                com.Parameters.AddWithValue("@password", user.password);
            }
            com.Parameters.AddWithValue("@created_at", user.created_at);
            com.Parameters.AddWithValue("@img_url", user.img_url);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
        public bool deleteUserAdmin(int id)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Delete from users where id = @id";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", id);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
        public string checkPasswordUserAdmin(int id)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string password = null;
            string sql = "select password from users where id = @id";

            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", id);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                password = (string)rd["password"];
                i++;
            }
            con.Close();
            if (i == 0)
            {
                return null;
            }
            return password;
        }
    }
}