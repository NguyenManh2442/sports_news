﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sports_News.Models
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}