﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Sports_News.Models
{
    public class PostEntities:DbContext
    {
        public PostEntities() : base("Sports_news")
        {
        }
        public DbSet<Post> Posts { get; set; }
        public int countPost()
        {
            int count = 0;
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT COUNT(id) as countId FROM posts";

            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            while (rd.Read())
            {
                count = (int)rd["countId"];
            }
            con.Close();
            return count;
        }
        public List<Post> showNewPosts()
        {
            List<Post> newPost = new List<Post>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT * FROM posts WHERE published = 0 ORDER BY created_at DESC";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                Post p = new Post();
                p.id = (int)rd["id"];
                p.title = (string)rd["title"];
                p.published = (byte)rd["published"];
                p.image = (string)rd["image"];
                p.value_category = (int)rd["value_category"];
                p.describes = (string)rd["describes"];
                p.body = (string)rd["body"];
                newPost.Add(p);
                i++;
            }
            con.Close();
            return newPost;
        }
        public List<Post> showPostWithCategory(int idCategory)
        {
            List<Post> newPost = new List<Post>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT * FROM posts Where value_category = " + idCategory + " AND published = 0 ORDER BY created_at DESC";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                Post p = new Post();
                p.id = (int)rd["id"];
                p.title = (string)rd["title"];
                p.published = (byte)rd["published"];
                p.image = (string)rd["image"];
                p.value_category = (int)rd["value_category"];
                p.describes = (string)rd["describes"];
                p.body = (string)rd["body"];
                newPost.Add(p);
                i++;
            }
            con.Close();
            return newPost;
        }
        public List<Post> showPostWithSearch(string valueSearch)
        {
            List<Post> newPost = new List<Post>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            valueSearch = "%" + valueSearch + "%";
            string sql = "SELECT * FROM posts Where title LIKE '" + valueSearch + "' AND published = 0";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                Post p = new Post();
                p.id = (int)rd["id"];
                p.title = (string)rd["title"];
                p.published = (byte)rd["published"];
                p.image = (string)rd["image"];
                p.value_category = (int)rd["value_category"];
                p.describes = (string)rd["describes"];
                p.body = (string)rd["body"];
                newPost.Add(p);
                i++;
            }
            con.Close();
            return newPost;
        }
        public List<Post> showPostDetail(int id)
        {
            List<Post> newPost = new List<Post>();
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "SELECT * FROM posts Where id = " + id + "";
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader rd = com.ExecuteReader();
            int i = 0;
            while (rd.Read())
            {
                Post p = new Post();
                p.id = (int)rd["id"];
                p.title = (string)rd["title"];
                p.published = (byte)rd["published"];
                p.image = (string)rd["image"];
                p.value_category = (int)rd["value_category"];
                p.describes = (string)rd["describes"];
                p.body = (string)rd["body"];
                newPost.Add(p);
                i++;
            }
            con.Close();
            return newPost;
        }
        public bool addPost(Post post)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Insert into posts(user_id, title, value_category, describes, body, published, image, created_at) values(@user_id, @title, @value_category , @describes, @body, @published, @image, @created_at) ";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@user_id", post.user_id);
            com.Parameters.AddWithValue("@title", post.title);
            com.Parameters.AddWithValue("@value_category", post.value_category);
            com.Parameters.AddWithValue("@describes", post.describes);
            com.Parameters.AddWithValue("@body", post.body);
            com.Parameters.AddWithValue("@published", post.published);
            com.Parameters.AddWithValue("@image", post.image);
            com.Parameters.AddWithValue("@created_at", post.created_at);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
        public bool editPost(Post post)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string sql = "";
            if (post.image == null)
            {
                sql = "Update posts Set user_id = @user_id, title = @title, value_category = @value_category, describes = @describes, body = @body, published = @published, created_at = @created_at Where id = @id ";
            }
            else
            {
                sql = "Update posts Set user_id = @user_id, title = @title, value_category = @value_category, describes = @describes, body = @body, published = @published, image = @image, created_at = @created_at Where id = @id ";
            }
            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", post.id);
            com.Parameters.AddWithValue("@user_id", post.user_id);
            com.Parameters.AddWithValue("@title", post.title);
            com.Parameters.AddWithValue("@value_category", post.value_category);
            com.Parameters.AddWithValue("@describes", post.describes);
            com.Parameters.AddWithValue("@body", post.body);
            com.Parameters.AddWithValue("@published", post.published);
            if(post.image != null)
            {
                com.Parameters.AddWithValue("@image", post.image);
            }
            com.Parameters.AddWithValue("@created_at", post.created_at);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
        public bool deletePost(int id)
        {
            string constr = ConfigurationManager.ConnectionStrings["Sports_news"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string sql = "Delete from posts where id = @id";

            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", id);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();
            return true;

        }
    }
}