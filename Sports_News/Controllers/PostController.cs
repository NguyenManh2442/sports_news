﻿using Sports_News.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sports_News.Controllers
{
    public class PostController : Controller
    {
        // GET: Product
        public ActionResult Show_post()
        {
            if(Convert.ToBoolean(Session.Contents["login"]))
            {
                PostEntities postEntities = new PostEntities();
                List<Post> posts = postEntities.Posts.ToList();
                return View(posts);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        public ActionResult Show_postWithCategory(string id)
        {
            PostEntities postEntities = new PostEntities();
            List<Post> posts = new List<Post>();
            posts = postEntities.showPostWithCategory(Convert.ToInt32(id));
            return View(posts);
        }
        //------------------------------------------
        public ActionResult Show_post_details(int id)
        {
            PostEntities postEntities = new PostEntities();
            Post post = postEntities.Posts.Single(p => p.id == id);
            return View(post);
        }
        public ActionResult Add_post()
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                CategoryEntities categoryEntities = new CategoryEntities();
                List<Category> categories = categoryEntities.Categories.ToList();
                return View(categories);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        //-----------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add_post(FormCollection formCollection, HttpPostedFileBase file)
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                byte pub = 1;
                string fileName = "";
                if (!String.IsNullOrEmpty(formCollection["published"]) && formCollection["published"] == "true")
                {
                    pub = 0;
                }
                if (ModelState.IsValid)
                {
                    try
                    {
                        if (file != null)
                        {
                            Random r = new Random();
                            fileName = r.Next(0, 100000) + r.Next(0, 100000) + file.FileName; 
                            string path = Path.Combine(Server.MapPath("~/Content/images"), Path.GetFileName(fileName));
                            file.SaveAs(path);
                        }
                        ViewBag.FileStatus = "File uploaded successfully.";
                    }
                    catch (Exception)
                    {
                        ViewBag.FileStatus = "Error while file uploading.";
                    }
                }
                Post post = new Post();
                post.user_id = Convert.ToInt32(Session["role"]);
                post.value_category = Convert.ToInt32(formCollection["category"]);
                post.title = formCollection["title"];
                post.published = pub;
                post.describes = formCollection["describes"];
                post.body = formCollection["body_post"];
                post.image = fileName;
                post.created_at = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz"));
                PostEntities postEntities = new PostEntities();
                postEntities.addPost(post);
                return RedirectToAction("Show_post");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        //-----------------------------
        public ActionResult Edit_post(int id)
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                PostEntities postEntities = new PostEntities();
                CategoryEntities categoryEntities = new CategoryEntities();
                MultipleModels mul = new MultipleModels();
                mul.Posts = postEntities.showPostDetail(id);
                mul.Categorys = categoryEntities.listCategory();
                return View(mul);
            }
            return RedirectToAction("About", "Home", new { area = "" });
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit_post(FormCollection formCollection, HttpPostedFileBase file)
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                byte pub = 1;
                string fileName = "";
                if (!String.IsNullOrEmpty(formCollection["published"]) && formCollection["published"] == "true")
                {
                    pub = 0;
                }
                if (ModelState.IsValid)
                {
                    try
                    {
                        if (file != null)
                        {
                            Random r = new Random();
                            fileName = r.Next(0, 100000) + r.Next(0, 100000) + file.FileName;
                            string path = Path.Combine(Server.MapPath("~/Content/images"), Path.GetFileName(fileName));
                            file.SaveAs(path);
                        }
                        else
                        {
                            fileName = null;
                        }
                        ViewBag.FileStatus = "File uploaded successfully.";
                    }
                    catch (Exception)
                    {
                        ViewBag.FileStatus = "Error while file uploading.";
                    }
                }
                Post post = new Post();
                post.id = Convert.ToInt32(formCollection["id"]);
                post.user_id = Convert.ToInt32(Session["id"]);
                post.value_category = Convert.ToInt32(formCollection["category"]);
                post.title = formCollection["title"];
                post.published = pub;
                post.describes = formCollection["describes"];
                post.body = formCollection["body_post"];
                post.image = fileName;
                post.created_at = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz"));
                PostEntities postEntities = new PostEntities();
                postEntities.editPost(post);
                return RedirectToAction("Show_post");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        public ActionResult Delete_post(int id)
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                PostEntities postEntities = new PostEntities();
                postEntities.deletePost(id);
                return RedirectToAction("Show_post");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
    }
}