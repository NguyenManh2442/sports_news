﻿using Sports_News.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sports_News.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Login(User formCollection)
        {
            if (!ModelState.IsValid)
            {
                return View(formCollection);
            }
            User user = new User();
            List<User> inforUser = new List<User>();
            user.email = formCollection.email;
            user.password = formCollection.password;
            UserEntities userEntities = new UserEntities();
            inforUser = userEntities.loginUser(user);
            if (inforUser != null)
            {
                
                int role = 0;
                foreach (User item in inforUser)
                {
                    if (BCrypt.Net.BCrypt.Verify(user.password, item.password))
                    {
                        Session["email"] = item.email;
                        Session["username"] = item.username;
                        Session["role"] = item.role;
                        Session["id"] = item.id;
                        Session["avatar"] = item.img_url;
                        role = item.role;
                    }
                    else
                    {
                        TempData["ErrP"] = "Tài khoản mật khẩu không chính xác!";
                        return View(formCollection);
                    }
                }
                Session["login"] = true;
                return RedirectToAction("Admin", "Home", new { area = "" });

            }
            
            return RedirectToAction("Login");
        }
        public ActionResult User_logout()
        {
            Session.Remove("username");
            Session.Remove("email");
            Session.Remove("role");
            Session.Remove("id");
            Session.Remove("avatar");
            Session["login"] = false;
            return RedirectToAction("Login");
        }
        public ActionResult Register()
        {
            return View();
        }
        //--------------------------------------------------------
        public ActionResult Show_userAdmin()
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                UserEntities userEntities = new UserEntities();
                List<User> users = userEntities.Users.ToList();
                return View(users);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        public ActionResult Add_userAdmin()
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                return View();
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        //-----------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add_userAdmin(FormCollection formCollection)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                if(formCollection["password"] == formCollection["confirm_password"])
                {
                    User user = new User();
                    user.email = formCollection["email"];
                    user.username = formCollection["username"];
                    user.full_name = formCollection["full_name"];
                    user.password = BCrypt.Net.BCrypt.HashPassword(formCollection["password"]);
                    user.role = Convert.ToInt32(formCollection["role"]);
                    user.created_at = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz"));
                    user.img_url = "";
                    UserEntities userEntities = new UserEntities();
                    userEntities.addUserAdmin(user);
                    return RedirectToAction("Show_userAdmin");
                }
                return View(formCollection);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        //------------------------------------------------------------

        public ActionResult Edit_userAdmin(int id)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                UserEntities userEntities = new UserEntities();
                User users = userEntities.Users.Single(p => p.id == id);
                return View(users);
            }
            return RedirectToAction("About", "Home", new { area = "" });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit_userAdmin(FormCollection formCollection)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                User user = new User();
                user.id = Convert.ToInt32(formCollection["id"]);
                user.email = formCollection["email"];
                user.username = formCollection["username"];
                user.full_name = formCollection["full_name"];
                user.role = Convert.ToInt32(formCollection["role"]);
                if (formCollection["new_password"] == null || formCollection["new_password"].Length <= 0)
                {
                    user.password = null;
                }
                else
                {
                    user.password = BCrypt.Net.BCrypt.HashPassword(formCollection["new_password"]);
                }
                user.created_at = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz"));
                user.img_url = "";
                UserEntities userEntities = new UserEntities();
                if (formCollection["password"] != null && formCollection["new_password"] != null && formCollection["confirm_password"] != null)
                {
                    userEntities.editUserAdmin(user);
                }
                else
                {
                    if (BCrypt.Net.BCrypt.Verify(formCollection["password"], userEntities.checkPasswordUserAdmin(user.id)))
                    {
                        userEntities.editUserAdmin(user);
                    }
                }
                return RedirectToAction("Show_userAdmin");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        public ActionResult Delete_userAdmin(int id)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                UserEntities userEntities = new UserEntities();
                userEntities.deleteUserAdmin(id);
                return RedirectToAction("Show_userAdmin");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        public ActionResult Edit_profile()
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                int id = Convert.ToInt32(Session["id"]);
                UserEntities userEntities = new UserEntities();
                User users = userEntities.Users.Single(p => p.id == id);
                return View(users);
            }
            return RedirectToAction("About", "Home", new { area = "" });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit_profile(FormCollection formCollection, HttpPostedFileBase file)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                string fileName = "";
                if (ModelState.IsValid)
                {
                    try
                    {
                        if (file != null)
                        {
                            Random r = new Random();
                            fileName = r.Next(0, 100000) + r.Next(0, 100000) + file.FileName;
                            string path = Path.Combine(Server.MapPath("~/Content/images"), Path.GetFileName(fileName));
                            file.SaveAs(path);
                        }
                        ViewBag.FileStatus = "File uploaded successfully.";
                    }
                    catch (Exception)
                    {
                        ViewBag.FileStatus = "Error while file uploading.";
                    }
                }
                User user = new User();
                user.id = Convert.ToInt32(formCollection["id"]);
                user.email = formCollection["email"];
                user.username = formCollection["username"];
                user.full_name = formCollection["full_name"];
                if (Convert.ToInt32(Session["role"]) == 1)
                {
                    user.role = Convert.ToInt32(formCollection["role"]);
                }
                else
                {
                    user.role = 0;
                }
                if (formCollection["new_password"] == null || formCollection["new_password"].Length <= 0)
                {
                    user.password = null;
                }
                else
                {
                    user.password = BCrypt.Net.BCrypt.HashPassword(formCollection["new_password"]);
                }
                user.created_at = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz"));
                user.img_url = fileName;

                UserEntities userEntities = new UserEntities();
                // check password co thay doi hay khong
                if (formCollection["password"] != null && formCollection["new_password"] != null && formCollection["confirm_password"] != null)
                {
                    userEntities.editUserAdmin(user);
                }
                else
                {   
                    // neu password thay doi thi check xem co chung voi password cu kh
                    if (BCrypt.Net.BCrypt.Verify(formCollection["password"], userEntities.checkPasswordUserAdmin(user.id)))
                    {
                        userEntities.editUserAdmin(user);
                    }
                }
                Session["username"] = formCollection["username"];
                Session["avatar"] = fileName;
                return RedirectToAction("Show_userAdmin");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
    }
}