﻿using Sports_News.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sports_News.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Show_category()
        {
            CategoryEntities categoryEntities = new CategoryEntities();
            List<Category> categories = categoryEntities.Categories.ToList();
            return View(categories);
        }
        public ActionResult Add_category()
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                return View();
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add_category(FormCollection formCollection)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                Category category = new Category();
                category.name = formCollection["name"];
                CategoryEntities categoryEntities = new CategoryEntities();
                categoryEntities.addCategory(category);
                return RedirectToAction("Show_category");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        public ActionResult Edit_category(int id)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                CategoryEntities categoryEntities = new CategoryEntities();
                Category category = categoryEntities.Categories.Single(p => p.id == id);
                return View(category);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit_category(FormCollection formCollection)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                Category category = new Category();
                category.name = formCollection["name"];
                category.id = Convert.ToInt32(formCollection["id"]);
                CategoryEntities categoryEntities = new CategoryEntities();
                categoryEntities.editCategory(category);
                return RedirectToAction("Show_category");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }

        public ActionResult Delete_category(int id)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["role"])) && Convert.ToInt32(Session["role"]) == 1)
            {
                CategoryEntities categoryEntities = new CategoryEntities();
                categoryEntities.deleteCategory(id);
                return RedirectToAction("Show_category");
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
    }
}