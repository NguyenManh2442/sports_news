﻿using Antlr.Runtime.Tree;
using Sports_News.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sports_News.Controllers
{
    public class HomeController : Controller
    {
        private UserEntities _db = new UserEntities();
        public ActionResult Index()
        {
            Session["login"] = false;
            List<Post> posts = new List<Post>();
            PostEntities postEntities = new PostEntities();
            posts = postEntities.showNewPosts();
            return View(posts);
        }

        public ActionResult About()
        {
            TempData["ErrorMessage"] = "Your application description page.";

            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection formCollection)
        {
            List<Post> posts = new List<Post>();
            PostEntities postEntities = new PostEntities();
            posts = postEntities.showPostWithSearch(formCollection["search"]);
            return View(posts);
        }
        public ActionResult Admin()
        {
            if (Convert.ToBoolean(Session.Contents["login"]))
            {
                PostEntities postEntities = new PostEntities();
                CategoryEntities categoryEntities = new CategoryEntities();
                UserEntities userEntities = new UserEntities();
                CountData cd = new CountData();
                cd.CountPosts = postEntities.Posts.Count();
                cd.CountPostPublic = postEntities.Posts.Where(x => x.published == 0).Count();
                cd.CountPostPrivate = cd.CountPosts - cd.CountPostPublic;
                cd.CountCategorys = categoryEntities.Categories.Count();
                cd.CountUsers = userEntities.Users.Count();
                cd.CountUserAdmin = userEntities.Users.Where(x => x.role == 1).Count();
                cd.CountUserEmp = cd.CountUsers - cd.CountUserAdmin;
                return View(cd);
            }
            return RedirectToAction("About", "Home", new { area = "" });
        }
        public ActionResult test()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult test(FormCollection formCollection)
        {
            int a = Convert.ToInt32(formCollection["a"]);
            int b = Convert.ToInt32(formCollection["b"]);
            int sum = a + b;
            TempData["sum"] = sum;
            return RedirectToAction("test", "Home", new { area = "" });
        }
    }
}